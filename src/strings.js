/**
 * Concat 2 strings
 * @return {String} The concatenated string
 */
function concat (a, b) {
  // TODO HERE
  return null
}

/**
 * Reverse a string
 * @return {String} The reversed string
 */
function reverse (s) {
  // TODO HERE
  return null
}

/**
 * Repeat a string 's', 'n' times, with each instance separated by 'sep'
 * @return {String} The concatenated string
 */
function repeat (s, n, sep) {
  // TODO HERE
  return null
}

/**
 * Return true if the string is a palindrome. Examples: madam, kayak, nurses run
 * Return false in any other cases including undefined, null, empty string
 * @param  {String}  s The input string
 * @return {Boolean}   Whether the string is a palindrome or not
 */
function isPalindrome (s) {
  // TODO HERE
  return false
}

/**
 * Convert a string to Camel case. For instance "hello world" into "helloWorld"
 * @param  {String} s Input string
 * @return {String}   Output string
 */
function camelCase (s) {
  // TODO HERE
  return s
}

/**
 * Convert a string from Camel case. For instance "helloWorld" into "hello world"
 * @param  {String} s Input string
 * @return {String}   Output string
 */
function unCamelCase (s) {
  // TODO HERE
  return s
}

/**
 * Check whether a given string is a pangram: ie a string containing all the 26 letters of the alphabet
 * @param  {String}  s Candidate string
 * @return {Boolean}   Result
 */
function isPangram (s) {
  if (s.length < 26) return false
  let res = true
  for (var i = 0; i < 26; i++) {
    let pattern = RegExp(String.fromCharCode(65 + i), 'gi')
    res = res && pattern.test(s)
    if (!res) break
  }
  return res
}

/**
 * Returns true if a SOS was found in the string
 * @param  {String}  s Potential SOS message
 * @return {Boolean}   Whether this is a SOS
 */
function isSOS (s) {
  // TODO
}

module.exports = {
  concat,
  reverse,
  repeat,
  isPalindrome,
  camelCase,
  unCamelCase,
  isPangram,
  isSOS
}
