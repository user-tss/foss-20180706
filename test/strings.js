/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */

import chai from 'chai'
import {
  concat,
  reverse,
  repeat,
  isPalindrome,
  camelCase,
  unCamelCase,
  isPangram,
  isSOS } from '../src/strings'
const should = chai.should()

describe('Strings', () => {
  xit('should concatenate 2 strings', () => {
    const res = concat('hello', ' world')
    should.exist(res)
    res.should.equal('hello world')
  })

  xit('should concatenate several strings', () => {
    const res = concat('hello', ' world', '!')
    res.should.equal('hello world!')
  })

  xit('should concat the empty string', () => {
    const res = concat('hello world', '')
    res.should.equal('hello world')
  })

  xit('should concat null string', () => {
    const res = concat('hello world', null)
    res.should.equal('hello world')
  })

  xit('should reverse a string', () => {
    const res = reverse('a1b2c3')
    res.should.equal('3c2b1a')
  })

  xit('should repeat a string', () => {
    const res = repeat('A', 3, ' ')
    res.should.equal('A A A')
  })

  xit('should repeat a string and handle a default separator', () => {
    const res = repeat('A', 3)
    res.should.equal('AAA')
  })

  describe('Palindromes', () => {
    const valid = ['kayak', 'madam', 'nurses run', ' nurses   run']
    valid.map(elem => {
      xit('should return true ', () => {
        isPalindrome(elem).should.be.true()
      })
    })

    const invalid = ['junk', null, undefined, '']
    invalid.map(elem => {
      xit('should return false ', () => {
        isPalindrome(elem).should.be.false()
      })
    })
  })

  describe('Camel case', () => {
    const toCamel = [{
      input: 'hello world',
      output: 'helloWorld'},
    {
      input: 'Hello World',
      output: 'helloWorld'},
    {
      input: 'this is camel case',
      output: 'thisIsCamelCase'}]
    toCamel.map(elem => {
      xit('should camel ' + elem.input, () => {
        camelCase(elem.input).should.equal(elem.output)
      })
    })

    const unCamel = [{
      input: 'helloWorld',
      output: 'hello world'},
    {
      input: 'thisIsCamelCase',
      output: 'this is camel case'}
    ]
    unCamel.map(elem => {
      xit('should uncamel ' + elem.input, () => {
        camelCase(elem.input).should.equal(elem.output)
      })
    })
  })

  describe('Pangrams', () => {
    const valid = [
      'We promptly judged antique ivory buckles for the next prize',
      'The quick brown fox jumps over the lazy dog'
    ]
    valid.map(elem => {
      xit('should be a pangram: ' + elem, () => {
        isPangram(elem).should.be.true
      })
    })

    const invalid = [
      'We promptly judged antique ivory buckles for the prize'
    ]
    invalid.map(elem => {
      xit('should not be a pangram: ' + elem, () => {
        camelCase(elem).should.be.false
      })
    })
  })

  describe('SOS', () => {
    const valid = [
      'SOS',
      'SOS!',
      'S.O.S.',
      'S  O  S',
      'S...O.......S...shrrrr bzzzz',
      'SHelpPleaseOhMyGodSNoooo!!!'
    ]
    valid.map(elem => {
      xit('should be a SOS: ' + elem, () => {
        isSOS(elem).should.be.true
      })
    })

    const invalid = [
      'SO...',
      '',
      null,
      undefined
    ]
    invalid.map(elem => {
      xit('should not be a SOS: ' + elem, () => {
        isSOS(elem).should.be.false
      })
    })
  })
})
